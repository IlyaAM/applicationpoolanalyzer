﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Timers;
using OfficeOpenXml;

namespace ApplicationPoolAnalyzer
{
    class Program
    {
        static string LogPath = @"C:/ApplicationPoolLogs";
        static object _locker = new Object();
        static void Main()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            var timer = new Timer();
            timer.Elapsed += (sender, args) => Start();
            timer.Interval = 30000;
            timer.AutoReset = true;
            timer.Start();

            Console.ReadKey();
        }

        static void Start()
        {
            lock (_locker)
            {
                var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\.."));
                var filePath = Path.Combine(path, "PoolList.txt");
                if (File.Exists(filePath))
                {
                    List<string> names = new();

                    foreach (var line in File.ReadAllLines(filePath).Where(c => !string.IsNullOrWhiteSpace(c)))
                    {
                        names.Add(line.ToLower());
                    }
                    try
                    {
                        ServerManager server = new ServerManager();

                        foreach (var pool in server.Sites.Where(x => names.Contains(x.Name.ToLower())))
                        {
                            var application = pool.Applications.FirstOrDefault(x => x.Path == "/");
                            var applicationPoolName = application.ApplicationPoolName;
                            var processId = server.WorkerProcesses.FirstOrDefault(x => x.AppPoolName == applicationPoolName)?.ProcessId;

                            if (!processId.HasValue) continue;

                            var process = Process.GetProcessById(processId.Value);

                            if (process == null) continue;

                            LogToFile(pool, process);

                            process.Dispose();
                        }

                        server.Dispose();

                        var folder = Path.Combine(LogPath, DateTime.Now.AddDays(-1).ToShortDateString());

                        if (DateTime.Now.Hour <= 2 && Directory.Exists(folder) && !File.Exists(Path.Combine(folder, "summary.xlsx")))
                        {
                            GenerateSummary(folder);
                        }

                        if (Directory.Exists(Path.Combine(LogPath, DateTime.Now.AddDays(-30).ToShortDateString())))
                        {
                            Directory.Delete(Path.Combine(LogPath, DateTime.Now.AddDays(-30).ToShortDateString()), true);
                        }
                    }
                    catch (Exception e)
                    {
                        CheckDirectories();

                        File.AppendAllText(Path.Combine(LogPath, DateTime.Now.ToShortDateString(), "errorLog.txt"), $"{DateTime.Now}. {e.Message} {e.StackTrace}" + Environment.NewLine);
                    }
                }
                else
                {
                    throw new Exception("PoolList.txt не найден");
                }
            }
        }

        static void LogToFile(Site pool, Process process)
        {
            CheckDirectories();

            long workingSet = process.WorkingSet64;

            //Console.WriteLine($"{DateTime.Now}. Процесс: {process.Id}. Проект: {pool.Name}. RAM: {workingSet / 1024 / 1024}. CPU: {GetCPUUsage(process)}." + Environment.NewLine);

            File.AppendAllText(Path.Combine(LogPath, DateTime.Now.ToShortDateString(), pool.Name + ".txt"),
                $"{DateTime.Now}. Процесс: {process.Id}. RAM: {workingSet / 1024 / 1024}. CPU: {GetCPUUsage(process)}." + Environment.NewLine);
        }

        static void CheckDirectories()
        {
            if (!Directory.Exists(LogPath))
                Directory.CreateDirectory(LogPath);

            if (!Directory.Exists(Path.Combine(LogPath, DateTime.Now.ToShortDateString())))
                Directory.CreateDirectory(Path.Combine(LogPath, DateTime.Now.ToShortDateString()));
        }

        static void GenerateSummary(string folder)
        {
            var files = Directory.GetFiles(folder).Where(x => x.Contains(".txt") && !x.Contains("errorLog.txt") && !x.Contains("summary.xlsx")).ToList();

            if (!files.Any())
                return;

            FileInfo fileInfo = new FileInfo(Path.Combine(folder, "summary.xlsx"));
            var package = new ExcelPackage(fileInfo);
            package.Workbook.Worksheets.Add("Отчет");
            ExcelWorksheet worksheet = package.Workbook.Worksheets["Отчет"];
            worksheet.Cells.AutoFitColumns();

            for (int i = 0; i < files.Count(); i++)
            {
                int k = 2;

                var project = Path.GetFileNameWithoutExtension(files[i]);
                var lines = File.ReadAllLines(files[i]);
                var excelSummary = new ExcelSummary();

                excelSummary.Project = project;

                foreach (var line in lines)
                {
                    var dateStr = line.Substring(0, 19);

                    var date = new DateTime();

                    if (DateTime.TryParse(dateStr, out DateTime _date))
                        date = _date;
                    else
                        continue;

                    var ram = GetBetween(line, "RAM:", ".").Trim();
                    var cpu = GetBetween(line, "CPU:", ".").Trim();

                    //if (excelSummary.ProcessId == 0 && int.TryParse(GetBetween(line, "Процесс:", ".").Trim(), out int result))
                    //    excelSummary.ProcessId = result;

                    if (int.TryParse(ram, out int _ramValue) && !excelSummary.RamValues.ContainsKey(date))
                        excelSummary.RamValues.Add(date, _ramValue);

                    if (int.TryParse(cpu, out int _cpuValue) && !excelSummary.CpuValues.ContainsKey(date))
                        excelSummary.CpuValues.Add(date, _cpuValue);
                }

                worksheet.Cells[1, 1].Value = DateTime.Now.AddDays(-1).ToShortDateString();
                worksheet.Cells[k++, i + 1].Value = $"{project}";

                if (excelSummary.RamValues.Any())
                {
                    var avg = Math.Round(excelSummary.RamValues.Select(x => x.Value).Average(), 1);
                    var maxAvg = Math.Round(excelSummary.RamValues.Select(x => x.Value).Max() / avg * 100, 1);
                    var minAvg = Math.Round(excelSummary.RamValues.Select(x => x.Value).Min() / avg * 100, 1);

                    worksheet.Cells[k++, i + 1].Value = $"Среднее ОЗУ: {avg} МБ";
                    worksheet.Cells[k++, i + 1].Value = $"Мин. ОЗУ: {excelSummary.RamValues.Select(x => x.Value).Min()} МБ ({minAvg}% от среднего)";
                    worksheet.Cells[k++, i + 1].Value = $"Макс. ОЗУ: {excelSummary.RamValues.Select(x => x.Value).Max()} МБ ({maxAvg}% от среднего)";
                }

                k++;

                if (excelSummary.CpuValues.Any())
                {
                    var avg = Math.Round(excelSummary.CpuValues.Select(x => x.Value).Average(), 1);
                    var maxAvg = Math.Round(excelSummary.CpuValues.Select(x => x.Value).Max() / avg * 100, 1);
                    var minAvg = Math.Round(excelSummary.CpuValues.Select(x => x.Value).Min() / avg * 100, 1);

                    worksheet.Cells[k++, i + 1].Value = $"Среднее ЦП: {avg}%";
                    worksheet.Cells[k++, i + 1].Value = $"Мин. ЦП: {excelSummary.CpuValues.Select(x => x.Value).Min()}% ({minAvg}% от среднего)";
                    worksheet.Cells[k++, i + 1].Value = $"Макс. ЦП: {excelSummary.CpuValues.Select(x => x.Value).Max()}% ({maxAvg}% от среднего)";
                }
            }

            package.Save();
        }

        static string GetBetween(string strSource, string strStart, string strEnd)
        {
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                int Start, End;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }

            return "";
        }

        static int GetCPUUsage(Process process)
        {
            var processName = GetProcessInstanceName(process.Id);

            if (string.IsNullOrWhiteSpace(processName))
                return 0;

            PerformanceCounter counter = new PerformanceCounter("Process", "% Processor Time", processName);

            var firstVal = counter.NextValue(); //служит началом отсчета, не убирать

            System.Threading.Thread.Sleep(500); //период, в течении которого замеряется % ЦП

            var secondVal = counter.NextValue(); //результат

            if (secondVal > 100)
                secondVal = 100;

            counter.Dispose();

            return (int)Math.Round(secondVal, 1);
        }

        static string GetProcessInstanceName(int pid)
        {
            PerformanceCounterCategory cat = new PerformanceCounterCategory("Process");

            string[] instances = cat.GetInstanceNames();
            foreach (string instance in instances)
            {
                using (PerformanceCounter cnt = new PerformanceCounter("Process", "ID Process", instance))
                {
                    if (cnt == null)
                        continue;

                    int val = (int)cnt.RawValue;
                    if (val == pid)
                    {
                        return instance;
                    }

                    return "";
                }
            }

            throw new Exception("Could not find performance counter instance name for current process. This is truly strange ...");
        }
    }

    public class ExcelSummary
    {
        public string Project { get; set; }
        public int ProcessId { get; set; }
        public Dictionary<DateTime, int> RamValues { get; set; } = new();
        public Dictionary<DateTime, int> CpuValues { get; set; } = new();
    }
}
